![Logo](/screenshots/Logo_new.png)

----------------------------------------------------

A fully custom Fire Emblem fangame created using the Lex Talionis engine (also developed by me). The engine (which is required to run this project) can be found here: https://gitlab.com/rainlash/lt-maker

Fire Emblem: The Lion Throne is a custom Fire Emblem fangame. Much of the inspiration for the game was drawn from the GBA and Tellius games. 

The Lion Throne has innovative objectives, powerful new items, custom classes, a fully functioning skill system with activated skills, a Tellius-style base menu, and much more!

#### To play:
The readme and wiki for the Lex Talionis engine itself (https://gitlab.com/rainlash/lt-maker) has much more information on how to get started. Once you have the engine set up, you can just download the *lion_throne.ltproj* above. The *lion_throne.ltproj* folder contains all the information the Lex Talionis engine needs to run the game, so just open it from within the Lex Talionis editor to get started.

### Screenshots
![TitleScreen](/screenshots/TitleScreen3.png) 
![Range](/screenshots/AOE2.gif)
![Skill](/screenshots/OphieSkill.gif)
![Prep](/screenshots/TheoSearch.gif)
![Conversation](/screenshots/Conversation1.png) 
![Convoy](/screenshots/Convoy1.png)
![Item](/screenshots/Item1.png) 
![Aura](/screenshots/Aura2.png)
![Base](/screenshots/Base2.png)

## Default Controls:

 - Arrow keys move the cursor.  
 - {X} is the 'A' button. Go forward.  
 - {Z} is the 'B' button. Go backward.  
 - {C} is the 'R' button. It gives the player additional info.  
 - {S} is the 'Start' button.  
 - {A} is the 'L' button.  

These defaults can be changed within the game.

## Background

While most people at this point are probably more familiar with the **Lex Talionis** engine qua engine than this little game, my original goal when I started programming back in 2013 was actually creating the Lion Throne **game**. The engine and the community came later.

The Lion Throne was created and developed solely by me (rainlash) between 2013 and 2016 while I was in college. I was unwilling to wait for FEXNA, so I decided to learn how to code a game on my own. While many of the features of the Lion Throne are commonplace today (2022) in the FE Fangame community, when I first got started that was not the case. Even as late as 2016, the community did not have Klok's graphics repo, FEBuilder, SkillSystem, or the wealth of game design knowledge available today. For some historical background, when the Lion Throne released in the summer of 2016, it was just the third completed English Fire Emblem custom fangame ever (after The Last Promise and Requiem).

Of course, the engine I developed to run the Lion Throne ended up becoming the much more popular of the pair, and with a lot of work by both me and the community it eventually evolved into LT-Maker (https://gitlab.com/rainlash/lt-maker). If you are interested in making your own Fire Emblem fangame, there's never been an easier time to jump in! If you have any questions, the LT discord is generally helpful: https://discord.gg/dC6VWGh4sw

### More Screenshots
![InfoMenu](/screenshots/InfoMenu2.png)
![Level5](/screenshots/Level5_2.png)
![TransitionScreen](/screenshots/TransitionScreen2.png)
![Combat](/screenshots/Combat1.png)
![Trade](/screenshots/Trade1.png)
![AOE](/screenshots/Range1.png)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
